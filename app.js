// Vaiables
const swipeUp = document.getElementById("swipeUp");
const loading = document.getElementById("loading");
const container = document.getElementById("container");
// This element contains weekListDays and switpeUp
const weekList = document.getElementById("weekList");
// This element just contains the week days weather info
const weekListDays = document.getElementById("weekListDays");

// These elemets can change depending if it's desktop or mobile
let cityName;
let cityTime;
let weatherName;
let currentTemp;
let weatherIcon;
let cityFinder;
let citiesList;
let searchBtn;
if (window.innerWidth >= 950) {
  weatherIcon = document.getElementById("weatherIcon");
  cityFinder = document.getElementById("cityFinder");
  citiesList = document.getElementById("citiesList");
  cityName = document.getElementById("cityName");
  cityTime = document.getElementById("cityTime");
  weatherName = document.getElementById("weatherName");
  currentTemp = document.getElementById("currentTemp");
  searchBtn = document.getElementById("searchBtn");
} else {
  weatherIcon = document.getElementById("mobileWeatherIcon");
  cityFinder = document.getElementById("mobileCityFinder");
  citiesList = document.getElementById("mobileCitiesList");
  cityName = document.getElementById("mobileCityName");
  cityTime = document.getElementById("mobileCityTime");
  weatherName = document.getElementById("mobileWeather");
  currentTemp = document.getElementById("mobileTemp");
  searchBtn = document.getElementById("mobileSearchBtn");
}

let initialY;
let currentY;
let height = 25;
const maxHeight = 80;
const minHeight = 25;
let isThrottled = false; //Control the Throttle
let defaultWoeid = localStorage.getItem("woeid") || 2487956;

// ---------------- Functions -----------
const debounce = (callback, wait = 100) => {
  let timeout;
  return function (...args) {
    clearTimeout(timeout);
    timeout = setTimeout(() => {
      callback.apply(this, args);
    }, wait);
  };
};

const throttle = (callback, wait) => {
  if (isThrottled) return;
  isThrottled = true;
  callback();
  setTimeout(() => (isThrottled = false), wait);
};

const getWeather = async (woeid) => {
  const res = await fetch(
    `https://secret-ocean-49799.herokuapp.com/https://www.metaweather.com/api/location/${woeid}/`
  );
  const data = await res.json();
  return data;
};

const changeBackground = (hour, weather) => {
  let dayTime;
  if (hour >= 19 || hour < 5) {
    dayTime = "night";
  } else if (hour >= 17) {
    dayTime = "evening";
  } else if (hour >= 12) {
    dayTime = "afternoon";
  } else if (hour >= 5) {
    dayTime = "morning";
  }
  container.style.backgroundImage = `url(./img/${dayTime}/${weather}-min.jpg)`;
};

const getCities = async (cityName) => {
  if (!cityName) return [];
  const res = await fetch(
    `https://secret-ocean-49799.herokuapp.com/https://www.metaweather.com/api/location/search/?query=${cityName}`
  );
  const data = await res.json();
  return data.map((item) => {
    if (item.location_type !== "City") return;
    return { cityName: item.title, woeid: item.woeid };
  });
};

const formatDate = (stringDate, timeZone, requestedData) => {
  switch (requestedData) {
    case "weekday":
      return new Date(stringDate).toLocaleString("en-US", {
        weekday: "long",
        timeZone,
      });
    case "hour":
      return new Date(stringDate).toLocaleString("es-MX", {
        hour: "2-digit",
        timeZone,
      });
    default:
      return new Date(stringDate).toLocaleString("en-US", {
        weekday: "long",
        hour: "2-digit",
        minute: "2-digit",
        timeZone,
      });
  }
};

const fillWeekWeather = (weekWeather, timezone) => {
  let innerHtml = "";
  for (let dayWeather of weekWeather) {
    innerHtml += `<div class="day-weather-info">
    <header>
      <p>${formatDate(dayWeather.applicable_date, timezone, "weekday")}</p>
      <p>${dayWeather.weather_state_name}</p>
    </header>
    <div class="day-temps">
      <div class="day-temp-info">
        <p class="day-temp">${Math.round(dayWeather.min_temp)}°</p>
        <p class="temp-description">Min Temp</p>
      </div>
      <div class="day-temp-info">
        <p class="day-temp">${Math.round(dayWeather.the_temp)}°</p>
        <p class="temp-description">Temp</p>
      </div>
      <div class="day-temp-info">
        <p class="day-temp">${Math.round(dayWeather.max_temp)}°</p>
        <p class="temp-description">Max Temp</p>
      </div>
    </div>
    <hr />
  </div>`;
  }
  weekListDays.innerHTML = innerHtml;
};

const selectCity = async (woeid) => {
  // Show the loading spinner
  loading.style.display = "flex";
  // Resets values
  citiesList.innerHTML = "";
  cityFinder.value = "";
  const cityData = await getWeather(woeid);
  const weekWeather = cityData.consolidated_weather;
  weatherIcon.setAttribute(
    "src",
    `https://www.metaweather.com/static/img/weather/${weekWeather[0].weather_state_abbr}.svg`
  );
  weatherIcon.setAttribute(
    "alt",
    `${weekWeather[0].weather_state_name} weather`
  );
  changeBackground(
    formatDate(cityData.time, cityData.timezone, "hour"),
    weekWeather[0].weather_state_abbr
  );
  cityName.innerText = cityData.title;
  cityTime.innerText = formatDate(cityData.time, cityData.timezone);
  weatherName.innerText = weekWeather[0].weather_state_name;
  currentTemp.innerText = `${Math.round(weekWeather[0].the_temp)}°`;
  fillWeekWeather(weekWeather, cityData.timezone);
  loading.style.display = "none";
  if (defaultWoeid === woeid) return;
  localStorage.setItem("woeid", woeid);
  defaultWoeid = woeid;
};

const fillCitiesList = (cities) => {
  citiesList.innerHTML = "";
  for (let city of cities) {
    const cityItem = document.createElement("p");
    cityItem.innerText = city.cityName;
    cityItem.addEventListener("click", () => selectCity(city.woeid));
    citiesList.append(cityItem);
  }
};

const selectCityByName = (cityName) => {
  throttle(async () => {
    const city = (await getCities(cityName)).pop();
    if (!city) return;
    selectCity(city.woeid);
  }, 10000);
};

//------------- Event listeners --------------
// ------------Swipe up week list-------------
swipeUp.addEventListener("touchstart", (e) => {
  initialY = e.touches[0].clientY;
});

swipeUp.addEventListener("touchmove", (e) => {
  e.preventDefault();
  currentY = e.touches[0].clientY;
  const tempHeight =
    height + ((initialY - currentY) / window.innerHeight) * 100;
  weekList.style.height = `${
    tempHeight < minHeight
      ? minHeight
      : tempHeight > maxHeight
      ? maxHeight
      : tempHeight
  }vh`;

  if (weekList.style.overflowY === "scroll" && tempHeight < maxHeight)
    weekList.style.overflowY = "hidden";
});

swipeUp.addEventListener("touchend", () => {
  const tempHeight =
    height + ((initialY - currentY) / window.innerHeight) * 100;
  height =
    tempHeight < minHeight
      ? minHeight
      : tempHeight > maxHeight
      ? maxHeight
      : tempHeight;
  if (
    height >= maxHeight &&
    (!weekList.style.overflowY || weekList.style.overflowY === "hidden")
  )
    weekList.style.overflowY = "scroll";
});

cityFinder.addEventListener("focus", () => {
  height = minHeight;
  weekList.style.transition = ".25s";
  weekList.style.height = `${minHeight}vh`;
  if (weekList.style.overflowY === "scroll")
    weekList.style.overflowY = "hidden";
  setTimeout(() => (weekList.style.transition = ""), 250);
});

cityFinder.addEventListener(
  "keyup",
  debounce(async (e) => {
    const cities = (await getCities(e.target.value)).slice(0, 3);
    fillCitiesList(cities);
  }, 400)
);

document.onkeypress = (e) => {
  if (e.key !== "Enter" || !cityFinder.value) return;
  selectCityByName(cityFinder.value);
};

searchBtn.addEventListener("click", () => {
  if (!cityFinder.value) return;
  selectCityByName(cityFinder.value);
});

selectCity(defaultWoeid);
